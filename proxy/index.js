const express = require('express');
const app = express();
const request = require('request-promise');
const path = require('path');

app.use(express.static(path.resolve(__dirname + '/../client')));

app.get('/proxy', function (req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET');

  request({
    url: req.query.url,
    method: 'GET'
  }).then(response => res.send(response))
    .catch(() => res.send(""));
});

let port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log('server running on ' + port);
});


