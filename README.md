# Task
- Write an application, which reads the content in HTML format from that URL: https://en.wikipedia.org/wiki/Europe
- Afterwards each link in that article MUST be stored and followed up, reads again the content from the link found and stores all of the URLs found in the content
- All of the processing MUST happen parallel (WebWorkers)
- At the end, create a responsive web page with a list of all URLs found, which can be ordered by URL name (ascending and descending, not taking into account the protocol prefix)
- Create unit tests for the relevant code parts
- The delivery MUST be a project, which contains all the tools and information to start directly from the command line / IDE
 

# Start
Install `node.js`

Run `npm install` and `npm run open`

# Test
Run `npm run test`