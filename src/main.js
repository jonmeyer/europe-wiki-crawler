import multiWorkers from './multiWorkers';
import appBuilder from './vueApp';

const ENTRY_PAGE = 'https://en.wikipedia.org/wiki/Europe';
const MAX_DEPTH = 1;
let workers = multiWorkers('js/worker.bundle.js', 8);
let app = appBuilder(() => workers.postMessage({url: ENTRY_PAGE, depth: 0}), () => workers.terminateAll());

workers.initWorkers(function(event) {
  let links = event.data.links;
  let depth = event.data.depth;
  app.linksToGet += depth <= MAX_DEPTH ? links.length - 1 : -1;
  app.allLinks.push(...event.data.links);
  links.forEach(link => {
    if (depth <= MAX_DEPTH) {
      workers.postMessage({url: link, depth: depth});
    }
  });
});
