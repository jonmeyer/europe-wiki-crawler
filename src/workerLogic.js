import htmlLinkFinderBuilder from './htmlLinkFinder';

export default function (callback) {
  return function (event) {
    let url = new URL(event.data.url);
    let htmlLinkFinder = htmlLinkFinderBuilder(url);
    let proxiedUrl = '/proxy?url=' + url;

    return fetch(proxiedUrl)
      .then(res => res.text())
      .then(htmlLinkFinder.findAllDistinctLinksInString)
      .then(links => callback({links, depth: event.data.depth + 1}))
      .catch(console.error);
  }
};