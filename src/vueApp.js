import SuiVue from "semantic-ui-vue";
import _ from 'lodash';
import Vue from 'vue';

export default function (loadLinks, terminateWorkers) {
  Vue.use(SuiVue);

  return new Vue({
    el: '#app',
    data: {
      linksToGet: 1,
      start: 0,
      linksPerPage: 20,
      allLinks: [],
      order: 'asc'
    },
    methods: {
      movePage: function (amount) {
        let newStart = this.start + (amount * this.linksPerPage);
        if (newStart > 0 && newStart < this.allLinks.length) {
          this.start = newStart;
        }
      },
      loadAllLinks: function (event) {
        loadLinks();
      },
      stopLoading: function () {
        this.linksToGet = 1;
        terminateWorkers();
      },
      changeOrder: function () {
        this.order = this.order === 'asc' ? 'desc' : 'asc';
      }
    },
    computed: {
      filteredLinks: function () {
        return this.allSorted.slice(this.start, this.linksPerPage + this.start);
      },
      allUnique: function () {
        return _.uniq(this.allLinks);
      },
      allSorted: function () {
        return _.orderBy(this.allUnique, [(l) => l.replace('https', '').replace('http', '')], [this.order]);
      },
      pages: function () {
        return Math.ceil(this.allUnique.length / this.linksPerPage)
      },
      currentPage: function () {
        return (this.start / this.linksPerPage) + 1
      }
    }
  });
};