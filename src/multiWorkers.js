const workers = function createWorkers(file, NUM_WORKERS = 1) {
  let _workers,
  _handler;

  function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  function postMessage(obj) {
    let index = randomInt(0, NUM_WORKERS - 1);
    obj.index = index;
    _workers[index].postMessage(obj);
  }

  function initWorkers(handler) {
    _handler = handler;
    _workers = [...Array(NUM_WORKERS)].map(() => new Worker(file));
    _workers.forEach(worker => worker.addEventListener('message', handler));
  }

  function terminateAll() {
    _workers.forEach(worker => worker.terminate());
    initWorkers(_handler);
  }

  return {
    postMessage,
    terminateAll,
    initWorkers
  }
};

export default workers;