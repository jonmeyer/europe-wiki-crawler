export default function (baseUrl) {

  function makeAbsolute(href) {
    if (baseUrl) {
      if (href.startsWith('//')) {
        return baseUrl.protocol + href;
      } else if (href.startsWith('/')) {
        return baseUrl.origin + href;
      } else {
        return href;
      }
    } else {
      return href;
    }
  }

  function findAllDistinctLinksInString(html) {
    let linkRegex = /<a([^>]+)>.*<\/a>/gi,
      hrefRegex = /href='([^#][^']*)'/i,
      match,
      links = new Set([]);

    while (match = linkRegex.exec(html)) {
      let linkAttributes = match[1].replace(/"/g, '\''),
        hrefMatch = linkAttributes.match(hrefRegex);

      if (hrefMatch && hrefMatch[1]) {
        links.add(makeAbsolute(hrefMatch[1]))
      }
    }

    return [...links];
  }

  return {
    findAllDistinctLinksInString
  }
};