import eventHandlerBuilder from './workerLogic';

self.addEventListener('message', eventHandlerBuilder(self.postMessage));