import appBuilder from '../src/vueApp';
import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
let expect = chai.expect;
chai.use(sinonChai);

describe('main vue app', () => {
  let loadSpy,
    terminateSpy,
    app;

  beforeEach(function () {
    loadSpy = sinon.spy();
    terminateSpy = sinon.spy();
    app = appBuilder(loadSpy, terminateSpy);
  });

  it('has default data', () => {
    expect(app.linksToGet).to.equal(1);
    expect(app.start).to.equal(0);
    expect(app.linksPerPage).to.equal(20);
    expect(app.allLinks).to.deep.equal([]);
    expect(app.order).to.equal('asc');
  });

  it('moves page forward', () => {
    app.allLinks = [...Array(100)].map((v,i) => "link" + i);

    app.movePage(1);

    expect(app.start).to.equal(20);
  });

  it('moves page backwards', () => {
    app.start = 40;
    app.allLinks = [...Array(100)].map((v,i) => "link" + i);

    app.movePage(-1);

    expect(app.start).to.equal(20);
  });

  it('returns current page', () => {
    app.start = 40;
    app.linksPerPage = 20;

    expect(app.currentPage).to.equal(3);
  });

  it('returns number of pages', () => {
    app.allLinks = [...Array(100)].map((v,i) => "link" + i);

    expect(app.pages).to.equal(5);
  });

  it('loads all links', () => {
    app.loadAllLinks();

    expect(loadSpy).to.have.been.called;
  });

  it('terminates workers on stop loading', () => {
    app.linksToGet = 200;
    app.stopLoading();

    expect(terminateSpy).to.have.been.called;
    expect(app.linksToGet).to.equal(1);
  });

  it('changes order', () => {
    app.changeOrder();

    expect(app.order).to.equal('desc');
  });

  it('returns unique links', () => {
    app.allLinks = ['hallo', 'eins', 'hallo'];

    expect(app.allUnique).to.deep.equal(['hallo', 'eins']);
  });

  it('returns sorted links', () => {
    app.allLinks = ['2zwei', 'https3drei', '1eins'];

    expect(app.allSorted).to.deep.equal(['1eins', '2zwei', 'https3drei']);
  });

  it('returns filtered links', () => {
    app.allLinks = [...Array(100)].map((v, i) => "link" + i);

    expect(app.filteredLinks).to.have.lengthOf(20);
    expect(app.filteredLinks[0]).to.equal("link0");
  });

  it('returns filtered links when paged', () => {
    app.allLinks = [...Array(100)].map((v, i) => "link" + (i < 10 ? "0" + i : i));
    app.start = 40;

    expect(app.filteredLinks).to.have.lengthOf(20);
    expect(app.filteredLinks[0]).to.equal("link40");
  });
});