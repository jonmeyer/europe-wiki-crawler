import htmlLinkFinderBuilder from '../src/htmlLinkFinder';
import { expect } from 'chai';


describe('without baseUrl', () => {
  let htmlLinkFinder = htmlLinkFinderBuilder();

  it('finds no links in empty string', () => {
    expect(htmlLinkFinder.findAllDistinctLinksInString("")).to.be.empty;
  });

  it('finds typical link', () => {
    expect(htmlLinkFinder.findAllDistinctLinksInString('<a href="aLink">name</a>')).to.deep.equal(["aLink"]);
  });

  it('finds link with apostrophe', () => {
    expect(htmlLinkFinder.findAllDistinctLinksInString("<a href='aLink'>name</a>")).to.deep.equal(["aLink"]);
  });

  it('only returns distinct links', () => {
    expect(htmlLinkFinder.findAllDistinctLinksInString("<a href='aLink'>name</a><a href='aLink'>otherName</a>")).to.deep.equal(["aLink"]);
  });

  it('skips internal # links', () => {
    expect(htmlLinkFinder.findAllDistinctLinksInString("<a href='#aLink'>name</a>")).to.be.empty;
  });

  it('find more than one link', () => {
    let input = `<div>some text</div>
  <a href="aLink">name</a><div>more html</div>
  <a href="anotherLink">name</a>
  <div>even more html</div>
  `;
    expect(htmlLinkFinder.findAllDistinctLinksInString(input)).to.deep.equal(["aLink", "anotherLink"]);
  });
});

describe('with baseUrl', () => {
  let htmlLinkFinder = htmlLinkFinderBuilder(new URL('https://some-domain.com'));

  it('leaves absolute as is', () => {
    expect(htmlLinkFinder.findAllDistinctLinksInString("<a href='https://some-other-domain.com/aLink'>name</a>"))
      .to.deep.equal(['https://some-other-domain.com/aLink']);
  });

  it('adds origin to relative links is', () => {
    expect(htmlLinkFinder.findAllDistinctLinksInString("<a href='/somePath/aLink'>name</a>"))
      .to.deep.equal(['https://some-domain.com/somePath/aLink']);
  });

  it('adds protocol to relative links is', () => {
    expect(htmlLinkFinder.findAllDistinctLinksInString("<a href='//some-domain.com/somePath/aLink'>name</a>"))
      .to.deep.equal(['https://some-domain.com/somePath/aLink']);
  });
});

