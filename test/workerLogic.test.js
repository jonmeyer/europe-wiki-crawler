import eventHandlerBuilder from '../src/workerLogic';
import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
let expect = chai.expect;
chai.use(sinonChai);


describe('logic for worker', () => {
  let callbackSpy;
  let fetchStub = sinon.stub(window, 'fetch');

  beforeEach(() => {
    callbackSpy = sinon.spy();
    fetchStub.reset();
  });

  it('returns function', () => {
    expect(eventHandlerBuilder(callbackSpy)).to.be.a('function');
  });

  it('fetches html and return links', () => {
    let event = {
      data: {
        url: 'https://google.de',
        depth: 0
      }
    };
    fetchStub.resolves(new Response("html with <a href='/link'>a link</a>"));

    return eventHandlerBuilder(callbackSpy)(event)
      .then(() => {
        expect(callbackSpy).to.have.been.calledWith({depth: 1, links: ['https://google.de/link']});
      });
  });

  it('fetches from proxy', () => {
    let event = {
      data: {
        url: 'https://google.de',
        depth: 0
      }
    };
    fetchStub.resolves('');
    eventHandlerBuilder(callbackSpy)(event);
    expect(fetchStub).to.have.been.calledWith('/proxy?url=https://google.de/');
  });
});