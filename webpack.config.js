const path = require('path');

module.exports = {
  entry: {
    main: './src/main.js',
    worker: './src/workerEntry.js'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'client/js')
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
    },
    extensions: [".js"],
    modules: [
      __dirname,
      path.resolve(__dirname, "./node_modules")
    ]
  }
};